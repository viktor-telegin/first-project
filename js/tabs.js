document.addEventListener('DOMContentLoaded',function(){
  document.querySelectorAll('.work__list-item__link').forEach(function(workListItemLink) {
    workListItemLink.addEventListener('click', function(event) {
      const path = event.currentTarget.dataset.path

      document.querySelectorAll('.work__content').forEach(function(workContent) {
        workContent.classList.remove('tab-content-active')
      })
      document.querySelectorAll('.work__list-item__link').forEach(function(workListItemLink) {
        workListItemLink.classList.remove('active-now')
      })
      document.querySelector(`[data-target="${path}"]`).classList.add('tab-content-active')
      document.querySelector(`[data-path="${path}"]`).classList.add('active-now')
    })
  })
})

//var header = document.getElementById("work__list");
//var workListItemLink = header.getElementsByClassName("work__list-item__link");
//for (var i = 0; i < workListItemLink.length; i++) {
  //workListItemLink[i].addEventListener("click", function() {
    //var current = document.getElementsByClassName("active-now");
    //current[0].className = current[0].className.replace(" active-now", "");
    //this.className += " active-now";
  //});
//}