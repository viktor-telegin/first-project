$( function() {
  $( "#accordion" ).accordion({
    collapsible: true,
    heightStyle: "content"
  });
  
} );

window.addEventListener('DOMContentLoaded', function() {
  document.querySelector('.faq__list-item').addEventListener('click',function(){
    document.querySelector('.faq__list-item').classList.remove('open'),
    document.querySelector('.faq__list-item').classList.add('open')
  })
})